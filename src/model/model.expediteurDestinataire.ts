export class Expediteur{
  nomExp:string="";
  prenomExp:string="";
  emailExp:string="";
  adressExp:string="";
  telExp:number=0;

  nomDes:string="";
  prenomDes:string="";
  emailDes:string="";
  adressDes:string="";
  telDes:number=0;
}
