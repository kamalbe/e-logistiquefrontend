import {Http} from '@angular/http';
import {Injectable} from '@angular/core';
import {Expediteur} from '../model/model.expediteurDestinataire';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Observable } from "rxjs/Observable";
import {Offre} from "../app/comparer/offre";



/*@Injectable()
export class  InformationService {

  private baseUrl = 'http://localhost:8091/offres';
  constructor(private http: Http) {
  }

  findAll(): Observable<Offre[]>  {
    return this.http.get(this.baseUrl)
      .map(res=>res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  saveExp(exp:Expediteur){

    return this.http.post("http://localhost:8090/expediteur",exp)
      .map(res=>res.json());
  }

}*/


@Injectable()
export class  InformationService {

  constructor(public http:Http){

  }

  getInformation(){
    return this.http.get("http://localhost:8086/chercherInformations")
      .map(res=>res.json());
  }


  saveExp(exp:Expediteur){

    return this.http.post("http://localhost:8086/expediteur",exp)
      .map(res=>res.json());
  }
}






