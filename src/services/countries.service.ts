import { Injectable } from '@angular/core';

import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";
import {Country} from "../model/Country";

@Injectable()
export class  CountriesService {

  constructor(public http:Http){

  }


    findAll(): Observable<Country[]>  {
      return this.http.get("http://localhost:8086/countries")
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }


}
