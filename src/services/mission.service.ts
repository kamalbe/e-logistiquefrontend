import { Injectable } from '@angular/core';
import {BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class MissionService {

  private messageSource = new BehaviorSubject('Vide');
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  changeMessage(DepartCountry: string) {
    this.messageSource.next(DepartCountry);
  }


}
