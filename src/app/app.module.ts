import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Menu1Component } from './menu1/menu1.component';
import { Menu2Component } from './menu2/menu2.component';

import { ComparerComponent } from './comparer/comparer.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {InformationService} from '../services/information.service';
import { CommandeComponent } from './commande/commande.component';

import {FormsModule} from '@angular/forms';
import { SuivisComponent } from './suivis/suivis.component';
import {CountriesService} from "../services/countries.service";

import {MissionService} from "../services/mission.service";
import { CreateAccountComponent } from './create-account/create-account.component';
import { LoginComponent } from './login/login.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap';
import { TestComponent } from './test/test.component';
import { ReactiveFormsModule } from '@angular/forms';

import { SearchService } from './search/search.service';


import { LetterBoldPipe } from './shared/letter-bold.pipe';
import { SearchFilterPipe } from './shared/filter-pipe';
import { ClickOutsideDirective } from './shared/dropdown.directive';
import { ApplicationRef } from '@angular/core';
import { ApiService } from './shared';
import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

const appRoutes:Routes=[
  {path:'offres',component:ComparerComponent},
  {path:'commande',component:CommandeComponent},
  {path:'suivis',component:SuivisComponent},
  {path:'Home',component:Menu2Component},
  {path:'Login',component:LoginComponent},
  {path:'Creation-compte',component:CreateAccountComponent},
  { path: '', redirectTo: '/Home', pathMatch: 'full' },
]

@NgModule({
  declarations: [
    AppComponent,
    Menu1Component,
    Menu2Component,
    ComparerComponent,
    CommandeComponent,
    SuivisComponent,
    CreateAccountComponent,
    LoginComponent,
    TestComponent,
    ClickOutsideDirective,
    SearchFilterPipe,
    LetterBoldPipe
  ],

  imports: [
    FormsModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    ReactiveFormsModule

  ],
  providers: [CountriesService,InformationService,MissionService,ApiService],
  bootstrap: [AppComponent]
})

export class AppModule {

  constructor(public appRef: ApplicationRef) {};
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
