import { Component, OnInit } from '@angular/core';
import {Expediteur} from '../../model/model.expediteurDestinataire';
import {InformationService} from '../../services/information.service';

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css']
})
export class CommandeComponent implements OnInit {

  exp:Expediteur=new Expediteur();
  mode:number=1;

  constructor(public informationService:InformationService) { }

  ngOnInit() {
  }


  saveExp(){
    this.informationService.saveExp(this.exp)
      .subscribe(data=>{
        this.exp=data;
        this.mode=2;
      },err=>{
        console.log(err)
      })
  }


}
