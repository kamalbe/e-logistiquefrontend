import { Component, OnInit,TemplateRef } from '@angular/core';
import {Http} from '@angular/http';
import "rxjs/add/operator/map";
import {MissionService} from "../../services/mission.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import {Offre} from "./offre";

@Component({
  selector: 'app-comparer',
  templateUrl: './comparer.component.html',
  styleUrls: ['./comparer.component.css']
})

/*export class ComparerComponent implements OnInit {

  offres:Offre[];

  constructor(public http: Http, public informationService: InformationService) {

  }

  ngOnInit() {
    this.getAllOffres();
  }

  getAllOffres() {
    this.informationService.findAll().subscribe(
      offres => {
        this.offres = offres;
      },
      err => {
        console.log(err);
      }
    );
  }
}*/



export class ComparerComponent implements OnInit {

  DepartCountry:string;
  DepartCity:string;
  ArrivedCountry:string;
  ArrivedCity:string;
  Poids:number;
  Longeur:number;
  Largeur:number;
  Hauteur:number;


  modalRef: BsModalRef;
  modalRef2: BsModalRef;
  config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false,
    class: 'gray modal-lg css'


  };

  constructor(public http:Http,private data:MissionService,private activatedRoute: ActivatedRoute,private modalService: BsModalService) {

  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  openModalTwo(template: TemplateRef<any>) {
    this.modalRef2 = this.modalService.show(template, this.config);
  }

  info(template: TemplateRef<any>,name:string) {
    this.modalRef = this.modalService.show(template);
  }
  ngOnInit() {
    document.body.classList.remove('bg-img');
  //  this.data.currentMessage.subscribe(message => this.message = message)
    this.activatedRoute
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.DepartCountry = params['DepartCountry'];
        this.DepartCity = params['DepartCity'];
        this.ArrivedCountry = params['ArrivedCountry'];
        this.ArrivedCity = params['ArrivedCity'];
        this.Poids = params['Poids'];
        this.Longeur = params['Longeur'];
        this.Largeur = params['Largeur'];
        this.Hauteur = params['Hauteur'];
      });
  }

   myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
  }


}






