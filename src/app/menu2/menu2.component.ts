import { Component, OnInit } from '@angular/core';
import {CountriesService} from '../../services/countries.service';
import {Country} from "../../model/Country";
import {MissionService} from "../../services/mission.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup} from "@angular/forms";
@Component({
  selector: 'app-menu2',
  templateUrl: './menu2.component.html',
  styleUrls: ['./menu2.component.css']
})


export class Menu2Component implements OnInit {

  countries: Country[];
  DepartCountry:string;
  DepartCity:string;
  ArrivedCountry:string;
  ArrivedCity:string;
  Poids:number;
  Longeur:number;
  Largeur:number;
  Hauteur:number;



  constructor(public country: CountriesService,public route:Router,private fb: FormBuilder) {
    this.initForm()
  }

  ngOnInit() {
    document.body.classList.add('bg-img');
    this.getAllCountries();
  }

  getAllCountries() {
    this.country.findAll().subscribe( data => {
      this.countries = data;
    });
  };

  onSubmit() {
     this.route.navigate(['/offres'],{queryParams:{DepartCountry:this.DepartCountry,DepartCity:this.DepartCity
     ,ArrivedCountry:this.ArrivedCountry,ArrivedCity:this.ArrivedCity,Poids:this.Poids,Longeur:this.Longeur
     ,Largeur:this.Largeur,Hauteur:this.Hauteur}});
  }
  stateForm: FormGroup;

  showDropDown = false;
  states = ['Paris', 'Pabu',  'Pace', 'Paars', 'Pact', 'Colorado',
    'Connecticut', 'Delaware', 'District of Columbia', 'Florida'
    , 'Georgia', 'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky'
    , 'Louisiana', 'Maine', 'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
    'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina',
    'North Dakota', 'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico',
    'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Island', 'Virginia', 'Washington',
    'West Virginia', 'Wisconsin', 'Wyoming'];



  initForm(): FormGroup {
    return this.stateForm = this.fb.group({
      search: [null]
    })
  }


  selectValue(value) {
    this.stateForm.patchValue({"search": value});
    this.showDropDown = false;
  }
  closeDropDown() {
    this.showDropDown = !this.showDropDown;
  }

  openDropDown() {
    this.showDropDown = false;
  }

  getSearchValue() {
    return this.stateForm.value.search;
  }
}


