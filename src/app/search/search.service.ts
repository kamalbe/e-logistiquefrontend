import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {City} from "../../model/City";


@Injectable()
export class SearchService {

  constructor(public http:Http){

  }
  search(id : number): Observable<City[]>  {
    return this.http.get("localhost:8086/citiesByCountry?idCountry="+id)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
