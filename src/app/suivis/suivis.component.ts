import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-suivis',
  templateUrl: './suivis.component.html',
  styleUrls: ['./suivis.component.css']
})
export class SuivisComponent implements OnInit {

  mode1:number=1;

  constructor() { }

  ngOnInit() {
  }

  rechercheSuivi(){
    this.mode1=2;
  }
}
